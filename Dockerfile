FROM debian:bookworm-slim

RUN apt update && apt -y install wget make git lsb-release software-properties-common gnupg && wget https://apt.llvm.org/llvm.sh && chmod +x llvm.sh && ./llvm.sh 18 && rm llvm.sh && apt -y install python3-pip && python3 -m pip install conan==2.6 cmake==3.30 --break-system-packages

WORKDIR /app
